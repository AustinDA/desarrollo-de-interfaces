﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClasesEstaticasVsNoestaticas
{
    static class ClassEstatica
    {
        public static int Estatica(int Bits) {
            return Bits*2;
        }
    }
}
