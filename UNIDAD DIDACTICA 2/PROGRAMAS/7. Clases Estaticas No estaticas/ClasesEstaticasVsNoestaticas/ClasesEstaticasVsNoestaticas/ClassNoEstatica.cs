﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace ClasesEstaticasVsNoestaticas
{
    class ClassNoEstatica
    {
        public int ClaseNoEstatica(int Bite) {
            return Bite * 2;
        }
    }
}
