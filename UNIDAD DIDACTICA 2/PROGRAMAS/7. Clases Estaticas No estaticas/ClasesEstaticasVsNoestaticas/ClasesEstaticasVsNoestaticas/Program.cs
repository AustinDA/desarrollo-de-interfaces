﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClasesEstaticasVsNoestaticas
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine(ClassEstatica.Estatica(5));  //La clase estatica no se puede instanciar

            ClassNoEstatica p = new ClassNoEstatica();
            Console.WriteLine(p.ClaseNoEstatica(10));  //La clase estatica se puede instanciar


            Console.ReadKey();
        }
    }
}
