﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TiposDeDatos
{
    class Program
    {
        static void Main(string[] args)
        {

            /*tipos de datos
             * https://docs.microsoft.com/es-es/dotnet/csharp/language-reference/builtin-types/value-types
             */

            //Integrales sbyte byte short ushort ademas de int
            int a; 

            //de coma flotante
            double b; //De ±5,0 × 10−324 a ±1,7 × 10308 8 bytes
            decimal d; //De ±1,0 x 10-28 to ±7,9228 x 1028 16 bytes
            float f; //De ±1,5 x 10-45 a ±3,4 x 1038 4 bytes

            f = 5.4f;
            d = 10.5m;
            b = 1.0D;

            //de texto

            string str = "Hola mi \n nombre es santy";
            string @bool = "mi nombre es santy";

            Console.WriteLine(str);
            Console.WriteLine(@bool);

            str = @"Hola mi \n nombre es santy";
            Console.WriteLine(str);

            Console.ReadKey();



        }
    }
}
