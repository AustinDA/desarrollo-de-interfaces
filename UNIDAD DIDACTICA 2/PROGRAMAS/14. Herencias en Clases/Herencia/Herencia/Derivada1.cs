﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Herencia
{
    class Derivada1: Base
    {
        public Derivada1(string nombre)
        {
            Nombre = nombre;
        }

        public void metodoDerivado() {
            Console.WriteLine("Este es mi nombre {0}", this.Nombre);
        }

        public override void IntApellido(string apellido)
        {
            base.IntApellido(apellido);
        }

    }
}
