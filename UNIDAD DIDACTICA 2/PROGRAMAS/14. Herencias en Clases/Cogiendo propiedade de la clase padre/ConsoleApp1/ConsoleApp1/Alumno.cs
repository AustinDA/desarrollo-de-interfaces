﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Alumno : Padrino
    {
        private static int contador;
        private int matricula;

        public static int Contador //método estatico me interesa llamarlo si necesidad de crear el objeto
        {
            get { return contador; } 
        }

       

        public Alumno(string p_nombre, int p_edad)
        {
            Edad = p_edad;
            Nombre = p_nombre;
            contador = ++contador;
            matricula = contador;
        }

        public int Edad { get; set; }

        public string Nombre { get; set; }

        public void Imprime()
        {
            Console.WriteLine("Nro.:" + matricula.ToString() + " de " + Contador.ToString() + " | Alumno: " + this.Nombre + " - Edad: " + this.Edad.ToString() + " años.");
        }






    }
}
