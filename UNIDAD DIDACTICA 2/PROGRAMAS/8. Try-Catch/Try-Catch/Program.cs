﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace Try_Catch
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Parte que pide un numero si no se entra un número produce una excepción
             * 
             * 
            Console.Write("Ingrese un valor:");
            string linea = Console.ReadLine();
            var num = int.Parse(linea);
            var cuadrado = num * num;
            Console.WriteLine($"El cuadrado de {num} es {cuadrado}");
            Console.ReadKey();
            *
            *
            */

            // CON EL TRY Y EL CATCH CONTROLAMOS LAS EXCEPCIONES

            try
            {
                Console.Write("Ingrese un valor:");
                string linea = Console.ReadLine();
                var num = int.Parse(linea);  //convertir cadena en numero
                var cuadrado = num * num;
                Console.WriteLine($"El cuadrado de {num} es {cuadrado}");
            }
            catch (FormatException e)
            {
                Console.Write("Debe ingresar obligatoriamente un número entero.");
                Console.Write(e.Message);
                
            }
            Console.ReadKey();

        }
    }
}
