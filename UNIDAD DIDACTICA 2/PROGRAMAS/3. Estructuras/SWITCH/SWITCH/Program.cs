﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWITCH
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Elija un número: 1, 2, 3");
           
            string eleccion = Console.ReadLine();
            int valor = Convert.ToInt32(eleccion);
            
            switch (valor)
            {
                case 1:
                    Console.WriteLine("Has elegido el 1");
                    break;
                case 2:
                    Console.WriteLine("Has elegido el 2");
                    break;
                case 3:
                    Console.WriteLine("Has elegido el 3");
                    break;
                case 4:
                    Console.WriteLine("Se que te has confundido y querias elegir el 1");
                    goto case 1;
                default:
                    Console.WriteLine("No válido. Escoja 1, 2, o 3.");
                    break;
            }

            Console.WriteLine("Gracias por elección");
            Console.ReadKey();
        }
    }
}
