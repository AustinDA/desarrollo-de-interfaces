﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private double grados;
        private void button1_Click(object sender, EventArgs e)
        {

            grados = Convert.ToSingle(textBox1.Text) * 9 / 5 + 32;
            textBox2.Text = grados.ToString();
        }
    }
}
