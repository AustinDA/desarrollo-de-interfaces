﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class FormNoModal : Form
    {
        private Form1 padre; 

        public FormNoModal(Form1 padre)
        {
            InitializeComponent();

            this.padre = padre;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            padre.MuestraLabel();
            this.Close();
        }
    }
}
